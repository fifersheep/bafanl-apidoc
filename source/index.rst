Welcome to bafanl-apidoc documentation!
=========================================

Contents:

.. toctree::
   :maxdepth: 2

   documents
   resources

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
