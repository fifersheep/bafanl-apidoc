=========
Documents
=========

The following are details of document formats seen in response bodies, with
attribute descriptions.

.. toctree::
    :glob:

    documents/*
