==============
Teams Resource
==============

Request Teams List
------------------

.. _get-teams:

.. http:get:: /teams/

    The teams collection resource - provides a list of teams.

    **Authentication required**: No

    **Example request**:

    .. sourcecode:: http

        GET /teams/ HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

    .. sourcecode:: json

        {
            "teams": [
                "*** Team Document ***",
                "*** Team Document ***"
            ]
        }



    :statuscode 200: No error
    :statuscode 404: Not Found

    **Documents**:

    * :doc:`../documents/team`

---------------------------------------

Request Single Team
-------------------

.. _get-team:

.. http:get:: /teams/{str:id}/

    The individual team resource. The ``id`` must be the team id.

    **Authentication required**: No

    Responds with team document for team id.

    **Example request**:

    .. sourcecode:: http

        GET /teams/48 HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

    .. sourcecode:: json

        {
            "teams": [
                "*** Team Document ***"
            ]
        }

    :param string id: A team id.

    :statuscode 200: No error
    :statuscode 404: Team not found

    **Documents**:

    * :doc:`../documents/team`

---------------------------------------

Create Team
-----------

.. _post-teams:

.. http:post:: /teams/

    Create a team with defining attributes.

    **Authentication required**: Yes

    A ``422`` status code will be returned with an error of ``TC.1`` for requests attempting
    creation with an existing id.

    A ``422`` status code will be returned with an error of ``TC.2`` error for requests where
    the team code is too long.

    **Example request**:

    .. sourcecode:: http

        POST /teams HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Content-Type: application/json
        Accept: application/json

    .. sourcecode:: json

        {
            "teams": [
                "*** Team Document ***"
            ]
        }

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 201 CREATED
        Content-Type: application/json

    :statuscode 201: One or more teams were successfully created
    :statuscode 401: Unauthorized
    :statuscode 422: No teams were created or the request could not be processed

    **Valid request body**:
    At least one team needs to be created. Must contain values for all attributes.

    **Errors**:

    The possible error messages, with codes, titles and descriptions are:

    +-----------+---------------------+------------------------------------------+
    | errorCode |   title             |    description                           |
    +===========+=====================+==========================================+
    | TC.1      | Existing ID         | The team id already exists.              |
    +-----------+---------------------+------------------------------------------+
    | TC.2      | Malformed Team Code | The team code is too long.               |
    +-----------+---------------------+------------------------------------------+


    **Documents**:

    * :doc:`../documents/team`

---------------------------------------

Delete Team
-----------

.. _post-teams-delete:

.. http:post:: /teams/{str:id}/delete/

    Delete a specific team using the team id.

    **Authentication required**: Yes

    Delete a team. Will respond with ``204 No Content`` on successful deletion.

    An unauthenticated user will produce ``404 Not Found``.

    If user is authenticated, but the team cannot be deleted for any reason
    then ``422 Forbidden`` is returned with an error object detailing the reason.

    **Example request**:

    .. sourcecode:: http

        POST /teams/48/delete HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Accept: application/json

    **Example response (deletion)**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

    .. sourcecode:: json

        {
            "teams": [
                "*** Team Document ***"
            ]
        }

    **Example response (non-deletion)**:

    .. sourcecode:: http

        HTTP/1.1 422 Forbidden
        Content-Type: application/json

    .. sourcecode:: json

        {
            "errors": [
                {
                    "code": "xxxx",
                    "title": "Team can not be deleted",
                    "description": "The team is protected"
                }
            ]
        }

    :statuscode 204: No Content
    :statuscode 401: Unauthorized
    :statuscode 422: Forbidden

    **Errors**:

    The possible error messages, with codes, titles and descriptions are:

    =========    ========================    ==========================
    errorCode    title                       description
    =========    ========================    ==========================
    CD.1         Team can not be deleted     The team is protected

    =========    ========================    ==========================
