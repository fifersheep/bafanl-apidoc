==================
Divisions Resource
==================

Request Divisions
-----------------

.. _get-divisions:

.. http:get:: /divisions/

    The divisions collection resource - provides a list of division summaries.

    **Authentication required**: No

    **Example request**:

    .. sourcecode:: http

        GET /divisions/ HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

    .. sourcecode:: json

        {
            "divisions": [
                "*** Division Summary Document ***",
                "*** Division Summary Document ***"
            ]
        }



    :statuscode 200: No error
    :statuscode 404: Not Found

    **Documents**:

    * :doc:`../documents/division`

---------------------------------------

Request Single Division
-----------------------

.. _get-division:

.. http:get:: /divisions/{str:id}/

    The individual division resource. The ``id`` must be the division id.

    **Authentication required**: No

    Responds with division full document for division id.

    **Example request**:

    .. sourcecode:: http

        GET /divisions/6/ HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

    .. sourcecode:: json

        {
            "division": {
                "*** Division Full Document ***"
            }
        }

    :param string id: A division id.

    :statuscode 200: No error
    :statuscode 404: Division not found

    **Documents**:

    * :doc:`../documents/division`

---------------------------------------

Create Division
---------------

.. _post-divisions:

.. http:post:: /divisions/

    Create a division with defining attributes.

    **Authentication required**: Yes

    A ``422`` status code will be returned with an error of ``DC.1`` for requests attempting
    creation with an existing id.

    **Example request**:

    .. sourcecode:: http

        POST /divisions/ HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Content-Type: application/json
        Accept: application/json

    .. sourcecode:: json

        {
            "divisions": [
                "*** Division Full Document ***"
            ]
        }

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 201 CREATED
        Content-Type: application/json

    :statuscode 201: One or more divisions were successfully created
    :statuscode 401: Unauthorized
    :statuscode 422: No divisions were created or the request could not be processed

    **Valid request body**:
    At least one division needs to be created. Must contain values for all attributes.

    **Errors**:

    The possible error messages, with codes, titles and descriptions are:

    +-----------+---------------------+----------------------------------------------+
    | errorCode |   title             |    description                               |
    +===========+=====================+==============================================+
    | DC.1      | Existing ID         | The division id already exists.              |
    +-----------+---------------------+----------------------------------------------+


    **Documents**:

    * :doc:`../documents/division`

---------------------------------------

Delete Division
---------------

.. _post-division-delete:

.. http:post:: /divisions/{str:id}/delete/

    Delete a specific division using the division id.

    **Authentication required**: Yes

    Delete a division. Will respond with ``204 No Content`` on successful deletion.

    An unauthenticated user will produce ``404 Not Found``.

    If user is authenticated, but the division cannot be deleted for any reason
    then ``422 Forbidden`` is returned with an error object detailing the reason.

    **Example request**:

    .. sourcecode:: http

        POST /divisions/7/delete/ HTTP/1.1
        Host: bafanl-api.eu-west-1.elasticbeanstalk.com/
        Accept: application/json

    **Example response (deletion)**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

    .. sourcecode:: json

        {
            "division": {
                "*** Division Summary Document ***"
        }

    **Example response (non-deletion)**:

    .. sourcecode:: http

        HTTP/1.1 422 Forbidden
        Content-Type: application/json

    .. sourcecode:: json

        {
            "errors": [
                {
                    "code": "xxxx",
                    "title": "Division can not be deleted",
                    "description": "The division is protected"
                }
            ]
        }

    :statuscode 204: No Content
    :statuscode 401: Unauthorized
    :statuscode 422: Forbidden

    **Errors**:

    The possible error messages, with codes, titles and descriptions are:

    =========    ===========================    ==========================
    errorCode    title                          description
    =========    ===========================    ==========================
    DD.1         Division can not be deleted    The division is protected

    =========    ===========================    ==========================
