=========
Resources
=========

A single resource represents a single object. Collection resources represent one
or more of these.

.. toctree::
    :glob:

    resources/*
