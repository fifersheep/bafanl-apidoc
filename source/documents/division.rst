=================
Division Document
=================

Division Summary Document
-------------------------

The division summary document.

.. sourcecode:: json

    {
        "id": "6",
        "code": "N2N",
        "name": "NFC II North"
    }

Division Full Document
----------------------

The full division document including teams.

.. sourcecode:: json

    {
        "id": "6",
        "code": "N2N",
        "name": "NFC II North",
        "teams": [
            "*** Team documents ***"
        ]
    }
