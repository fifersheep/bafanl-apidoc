=============
Team document
=============

Team Summary Document
---------------------

The standard team document containing all team attributes.

.. sourcecode:: json

    {
        "city": "Edinburgh",
        "code": "EDI",
        "id": "48",
        "name": "Wolves"
    }
