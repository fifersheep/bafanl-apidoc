===============
Player Document
===============

Player Full Document
--------------------

The full player document containing all attributes.

.. sourcecode:: json

    {
        "id": "369",
        "first_names": "Eric",
        "last_name": "Rubin",
        "nickname": "Big Nipple",
        "position": "QB",
        "team_id": "1",
        "number": 7
    }
